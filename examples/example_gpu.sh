#!/bin/bash
#
# Visual Geometry Group, University of Oxford (2016).
#
# Example command line usage.
#
# This example assumes that MATLAB (either the full version or the runtime version)
# is installed at /sync/MATLAB/R2016a. IT IS IMPORTANT TO CHANGE THIS PATH TO THE
# ACTUAL LOCATION OF MATLAB/ MATLAB Runtime.
# Please read the readme.txt for detailed installation instructions.

# To use the GPU #1, we give the gpu device number as 1.
# (indexed from 1: i.e., the first GPU device is referred 


matroot=/sync/MATLAB/R2016a # << CHANGE THIS TO POINT TO THE LOCATION OF MATLAB [runtime]
input=frames.txt # this is a text with a list of input frames
output=out.xml   # this is the name of the output file generated

../run.sh $matroot --input $input --output $output --mode opt --verbose --gpuID 1

